﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class PlayerState
{
    private int lives;
    private int money;
    private int diamond;

    public PlayerState()
    {
    }

    public PlayerState(int lives, int money, int diamond)
    {
        this.lives = lives;
        this.money = money;
        this.diamond = diamond;
    }

    public int Lives
    {
        get { return lives; }
        set
        {
            if (value < 5) lives = value;
        }
    }
    public int Money
    {
        get { return money; }
        set { money = value; }
    }
    public int Diamond
    {
        get { return diamond; }
        set { diamond = value; }
    }
}

