﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using System.Xml.Serialization;

public class PlayerScript : Unit
{
    [SerializeField]
    public float speed = 3.0F;
    [SerializeField]
    public float jumpForce = 15.0F;

    private LivesBar livesBar;
    private Status status;
    public bool isGrounded = false;
    private Bullet bullet;
    private Heart heart;
    private State state;
    private IdleState idleState;
    public GameOriginator startGame;

    // Для зміни анімації
    public CharState State
    {
        get { return (CharState)animator.GetInteger("State"); }
        set { animator.SetInteger("State", (int)value); }
    }

    new public Rigidbody2D rigitBody;
    private Animator animator;
    public SpriteRenderer sprite;

    // Отримання вказівників
    private void Awake()
    {
        livesBar = FindObjectOfType<LivesBar>();
        status = FindObjectOfType<Status>();
        rigitBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();

        bullet = Resources.Load<Bullet>("Bullet");
        heart = Resources.Load<Heart>("Heart");

        if (GlobalVariables.isNewGame == true)
        {
            startGame = new GameOriginator();
        }
        if (GlobalVariables.isNewGame == false)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(PlayerState));
            PlayerState playState;

            using (FileStream fs = new FileStream("state.xml", FileMode.OpenOrCreate))
            {
                playState = (PlayerState)formatter.Deserialize(fs);

            }
            PlayerState state1 = new PlayerState(4, 96, 3);
            GameMemento savedGame = new GameMemento(playState);
            startGame = new GameOriginator();
            startGame.LoadGame(savedGame);

            //Debug.Log("Lives - " + startGame.state.Lives + "\nMoney - " + startGame.state.Money + "\nDiamond - " + startGame.state.Diamond);

            //status.Refresh();
            //livesBar.Refresh();
        }

        Vector3 position = transform.position;
        position.x = -10F;
        position.y = -2.5F;
        Instantiate(heart, position, heart.transform.rotation);
        heart.setIsDestroyed(false);

        idleState = new IdleState();
        state = idleState;
    }

    private void Start()
    {

    }
    // Постійна перевірка черкання землі
    private void FixedUpdate()
    {
        CheckGround();
    }

    private void Update()
    {
        if (isGrounded) state = idleState;
        if (Input.GetButtonDown("Fire1")) Shoot();
        if (Input.GetButton("Horizontal")) state = new RunState();
        if (isGrounded && Input.GetButtonDown("Jump")) state = new JumpState();
        if (Input.GetKeyDown(KeyCode.F4))
        {
            XmlSerializer formatter = new XmlSerializer(typeof(PlayerState));

            // получаем поток, куда будем записывать сериализованный объект
            using (FileStream fs = new FileStream("state.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, startGame.GameSave());
            }
        }

        state.Execute(this);

        if (heart.getIsDestroyed())
        {
            int selected = Random.Range(0, 3);
            Vector3 position = transform.position;

            switch (selected)
            {
                case 0:
                    position.x = -10F;
                    position.y = -2.5F;
                    break;
                case 1:
                    position.x = 15.95F;
                    position.y = -2.5F;
                    break;
                case 2:
                    position.x = -3.55F;
                    position.y = -2.5F;
                    break;
                default:
                    position.x = 5.7F;
                    position.y = -0.65F;
                    break;
            }
            Instantiate(heart, position, heart.transform.rotation);
            heart.setIsDestroyed(false);
        }

        livesBar.Refresh();
        status.Refresh();
    }

    //private void Run()
    //{
    //    Vector3 direction = transform.right * Input.GetAxis("Horizontal");

    //    // Рухається звідки, куда + напрям, скорість*час між фреймами (поточним і минулим)
    //    transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);

    //    sprite.flipX = direction.x < 0;
    //    if (isGrounded) State = CharState.Run;
    //}

    //private void Jump()
    //{
    //    if (isGrounded) State = CharState.Jump;
    //    // властивість rigitBody - надавати кудась силу
    //    rigitBody.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
    //}

    private void Shoot()
    {
        Vector3 position = transform.position; position.y += 0.5F;
        Bullet newBullet = Instantiate(bullet, position, bullet.transform.rotation) as Bullet; //створення кулі

        newBullet.Parent = gameObject; //встановлення батька персонажа, який випустив кулю
        newBullet.Direction = newBullet.transform.right * (sprite.flipX ? -1.0F : 1.0F); // зміна напряму кулі
    }

    // Перевірка черкання землі
    private void CheckGround()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.3F);

        isGrounded = ((colliders.Length > 1) && colliders.All(x => !x.GetComponent<Bullet>())); // Якщо персонаж черкає більше одного елемента значить він на землі (перший завжди існує, тощу він черкає себе)
        if (!isGrounded) State = CharState.Jump;
    }

    public override void ReceiveDamage()
    {
        if (startGame.state.Lives > 0)
        {
            startGame.state.Lives = startGame.state.Lives - 1;
            startGame.state.Money = startGame.state.Money - 1;
            status.Refresh();
            livesBar.Refresh();
        }

        rigitBody.AddForce(transform.up * 6.0F, ForceMode2D.Impulse);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Bullet bullet = collision.gameObject.GetComponent<Bullet>();
        Heart heart1 = collision.gameObject.GetComponent<Heart>();

        if (bullet && bullet.Parent != gameObject)
        {
            ReceiveDamage();
        }
        if (heart1)
        {
            startGame.state.Lives = startGame.state.Lives + 1;
            livesBar.Refresh();
            heart.setIsDestroyed(true);
        }
    }
}

//--------------------------------------------------------------------------------

public enum CharState
{
    Idle,
    Run,
    Jump
}

//--------------------------------------------------------------------------------

public class Money
{
    private int countOfMoney;
    private int countOfDiamond;

    public Money(int countOfMoney, int countOfDiamond)
    {
        this.countOfMoney = countOfMoney;
        this.countOfDiamond = countOfDiamond;
    }

    public void setMoney(int money)
    {
        this.countOfMoney = money;
    }
    public void setDiamond(int diamonds)
    {
        this.countOfDiamond = diamonds;
    }
    public int getMoney()
    {
        return countOfMoney;
    }
    public int getDiamond()
    {
        return countOfDiamond;
    }

    public void addMoney()
    {
        countOfMoney++;
    }
    public void addDiamond()
    {
        countOfDiamond++;
    }
}

//--------------------------------------------------------------------------------

public class Level
{
    private int level;

    public Level()
    {
        level = 1;
    }

    public int getLevel()
    {
        return level;
    }

    public void nextLevel()
    {
        level++;
    }
}

//--------------------------------------------------------------------------------

public class LevelsTime
{
    private int time;
    private Level level;

    public LevelsTime(Level level)
    {
        this.level = level;

        TimeForSelectedLevel(level.getLevel());
    }

    public void nexLevel()
    {
        level.nextLevel();
    }

    public void TimeForSelectedLevel(int currentLEvel)
    {
        switch (currentLEvel)
        {
            case 1:
                time = 60000;
                break;
            case 2:
                time = 55000;
                break;
            case 3:
                time = 50000;
                break;
            case 4:
                time = 45000;
                break;
            case 5:
                time = 40000;
                break;
        }
    }

    public int getTime()
    {
        return time;
    }
}

//--------------------------------------------------------------------------------