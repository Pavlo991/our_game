﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : Unit
{
    private bool isDestroy = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Unit unit = collision.GetComponent<Unit>();

        if (unit)
        {
            Destroy(gameObject);
            isDestroy = true;
        }
    }

    public void setIsDestroyed(bool isDestroy)
    {
        this.isDestroy = isDestroy;
    }
    public bool getIsDestroyed()
    {
        return isDestroy;
    }
}
