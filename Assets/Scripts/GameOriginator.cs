﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class GameOriginator
{
    public PlayerState state = new PlayerState(5, 100, 5);

    public PlayerState GameSave()
    {
        return state;
    }
    public void LoadGame(GameMemento memento)
    {
        state = memento.GetState();
    }
}

