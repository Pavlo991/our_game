﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class JumpState : State
{
    public override void Execute(PlayerScript character)
    {
        if (character.isGrounded) character.State = CharState.Jump;
        // властивість rigitBody - надавати кудась силу
        character.rigitBody.AddForce(character.transform.up * character.jumpForce, ForceMode2D.Impulse);
    }
}
