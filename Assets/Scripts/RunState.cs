﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class RunState : State
{
    public override void Execute(PlayerScript character)
    {
        Vector3 direction = character.transform.right * Input.GetAxis("Horizontal");

        // Рухається звідки, куда + напрям, скорість*час між фреймами (поточним і минулим)
        character.transform.position = Vector3.MoveTowards(character.transform.position, character.transform.position + direction, character.speed * Time.deltaTime);

        character.sprite.flipX = direction.x < 0;
        if (character.isGrounded) character.State = CharState.Run;
    }
}
