﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

abstract class State
{
    public abstract void Execute(PlayerScript character);
}

