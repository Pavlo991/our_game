﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class GameMemento
{
    private PlayerState state;

    public GameMemento(PlayerState state)
    {
        this.state = state;
    }

    public PlayerState GetState()
    {
        return state;
    }
}

